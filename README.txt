$Id

-----------
TO INSTALL:
-----------
1. Make sure you have installed the latest version of CCK (http://drupal.org/project/cck)
2. Copy this module into your sites/all/modules, sites/default/modules or sites/sitename.com/modules directory
3. Go to the module page (http://sitename.com/admin/build/modules)
4. Under the CCK group, check 'Striptags Format', then submit the form.
5. Enable this as the field format in the 'Display Fields' area (http://sitename.com/admin/content/node-type/NODE-TYPE-NAME/display) 
   or select it as the display format for a particular field in Views.
6. See your no-X/HTML enabled fields!

------
NOTES:
------
This module has been submitted to the CCK issue queue for possible inclusion in the next major release.